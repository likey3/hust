var app = angular.module('userApp', ['ngCookies']);

pageType = "user";
taskType = "user";

app.controller('navCtrl', navController)
.controller('centerCtrl', function ($scope, $http, $rootScope) {
	$scope.content = getContentURL(2);
	$scope.show_notice = false;

	$scope.changeContent = function(page) {
		$scope.content = getContentURL(page);
	}

	$rootScope.showMessage = function(content, isShow) {
		$scope.show_notice = isShow;
		$scope.notice_message = content;
	}

	$scope.confirmMessage = function() {
		$scope.show_notice = false;
	}
})
.controller('publishCtrl', function($scope, $http, $rootScope) {
	$scope.taskTitle = "";
	$scope.taskContent = "";

	$http.get('api/task/category/list').success(function (data) {
		console.log('taskCategory:', data);
		data.shift(1);
		console.log(data);
		$scope.taskCategory = data;
	});

	$scope.publish = function(){
		var taskInfo = {
			"title": $scope.taskTitle,
			"content": $scope.taskContent,
			"category": $scope.category.key
		}

		console.log(taskInfo);
		$http.post('api/task/publish', taskInfo)
		.success(function(data) {
			if (data.status == 1) {
				//alert('发布成功');
				$rootScope.showMessage('发布成功', true);
				$scope.taskTitle = "";
				$scope.taskContent = "";
				$scope.category = "";
			}
			console.log(data);
		});
	}
})
.controller('infoCtrl', function($scope, $http, $rootScope) {
	$scope.username = "";
	$scope.tel = "";
	$scope.sex = "";
	$scope.age = "";
	$scope.updateUserInfo = function() {
		var userInfo = {
			"age": $scope.age,
			"sex": $scope.sex,
			"major": $scope.major
		};

		console.log(userInfo);

		$http.post('api/user/update', userInfo)
		.success(function(data) {
			console.log(data);
			if (data.status == 1) {
				//alert('个人信息修改成功!');
				$rootScope.showMessage('个人信息修改成功!', true);
				getUserInfo();
			} else {
				$rootScope.showMessage(data.msg, true);
			}
		});
	}

	getUserInfo();

	function getUserInfo() {
		var userInfo = {"username": "wangxi"};

		$http.post('api/user/info', userInfo).success(function(data){
			console.log(data);
			var msg = data.msg;
			$scope.username = msg.username;
			$scope.tel = msg.phonenumber;
			$scope.age = msg.age;
			$scope.sex = msg.sex;
			$scope.major = msg.major;
		});
	}

})
.controller('taskCtrl', taskController)
.controller('taskTypeCtrl', function ($scope, $http, $rootScope) {
	$rootScope.taskList = [];

	$scope.getTask = function(taskType) {
		if (taskType == 1) {
			$rootScope.taskType = 1;
		} else {
			$rootScope.taskType = 0;
		}

		$rootScope.getUserTask(taskType);
	}
});

function getContentURL(page) {
	if (page == 2) {
		return 'views/main.publish.html';
	} else if (page == 3) {
		return 'views/main.task.html';
	} 
	else if (page == 4) {
		return 'views/main.info.html';
	} else {
		return 'views/main.publish.html';
	}
}
