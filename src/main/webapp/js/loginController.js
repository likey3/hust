var login=angular.module('login', ['ngCookies']);
pageType = 'login';

login.controller('loginCtrl', function ($scope, $http, $cookies) {
	$scope.username = "";
	$scope.password = "";
	$scope.notice_username = "";
	$scope.notice_password = "";
	// $scope.usernameError = true;
	// $scope.usernameErrorMsg = "init";
   var isOk=true;
	/*未输入用户名就点击下面的输入框*/ 
	$scope.nameVali=function() {     
		if($scope.username.length<1){
			$scope.notice_username="请输入用户名";
			isOk = false; 
		}   
	}
	$scope.passwordVali=function() {     
    	if($scope.password.length<1){
  		$scope.notice_password="请输入密码";
  		isOk = false; 
    	}   
       }


	$scope.submit = function() {
		var loginInfo = {
			"username": $scope.username, 
			"password": $scope.password
		};

    //validation
        
    	if($scope.username.length<1){
  		$scope.notice_username="请输入用户名";
  		isOk = false; 
    	}   
        
    	if($scope.password.length<1){
  		$scope.notice_password="请输入密码";
  		isOk = false; 
    	}  
    	if (!isOk) {
          return;
        }
        document.getElementsByClassName("loading")[0].style.display="block"; 
       

	//show dialog()
	$http.post('api/user/login', loginInfo)
	.success(function (data) {
			// close dialog()
			console.log(data);
			if (data.status === 1) {
				console.log("loginCookie: ", $cookies.get('username'));
				window.location = "index.html";
			} else {
				document.getElementsByClassName("loading")[0].style.display="none";
				console.log(data.msg);
				//...todo
			}
		});
	}

	// $http.post('api/user/login', loginInfo )
	// .success(function(data) {
	// 	if (data.status == 1) {
	// 		window.location = "index.html";
	// 	} else if (data.status == 0) {
	// 		document.getElementsByClassName("loading")[0].style.display="none";
	// 		$scope.error_notice=data.msg;       
	// 	}
	// })
	// .error(function(data) {

	// });


	// $scope.$watch('username', function(newVal, oldVal) {
	// 	$scope.usernameError = true;	
	// 	if (newVal.length == 0) {
	// 		$scope.usernameErrorMsg = 'tel not allow empty';
	// 	} else if (newVal.length != 11) {
	// 		$scope.usernameErrorMsg = 'not 11';
	// 	} else if (!angular.isNumber(newVal)) {
	// 		$scope.usernameErrorMsg = 'tel must be number';
	// 	} else {
	// 		$scope.usernameError = false;
	// 	}
	// });

	// $scope.$watch('password', function(newVal, oldVal) {
	// 	if (newVal.length < 6) {

	// 	}
	// })
})
.controller('navCtrl', navController);