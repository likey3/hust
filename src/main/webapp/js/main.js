$(window).ready(function(){
	var controller={//左侧页面切换控制器
		debug:true,//测试阶段
		usr_id:-1,
		usr_token:"",
		page:1,
		pageId:[null,"publish","mytask","info"],
		init:function(){
			//绑定界面切换事
			$.each([1,2,3],function(key,i){
				$("#sel"+i).bind("click",function(){
					controller.selectPage(i);
				});
			});
			//从cookie获取usr_id及校验token

			//定位到界面1
			this.selectPage(1);
		},
		selectPage:function(p) {//选择页面
			for(var i=1;i<=3;i++)	if(i!=p){//非选择
				$("#"+this.pageId[i]).hide(0);
				$("#sel"+i+" li").css("color","black");
				$("#sel"+i+" li").css("font-weight","normal");
			}
			//选择:
			$("#sel"+p+" li").css("color","blue");
			$("#sel"+p+" li").css("font-weight","bold");
			$("#"+this.pageId[p]).show(0,view.content[p].init);//加载页面并初始化
			this.page=p;
		}
	};
	var view={
		content:[//视图内容
			null,//0
			{},//page1  一键发布
			{//page2  我的任务
				init:function(){
					function succ(task){
						
					}
					if(!controller.debug) view.ajax("",{},this.succ,this.error);//网络获取
					else succ({//测试
						
					});
				}
			},
			{//page3  个人信息
				init:function(){
					if(!controller.debug) view.ajax("",{},this.succ,this.error);//网络获取
				}
			}
		],
		ajax:function(url,data,succ,error){
			try{
				$.post(url,data,function(data,succ){
					if(200==status){//处理json
						try{
							if(data instanceof String) data=JSON.parse(data);
							succ(data);
						}catch(e){
							error();
						}
					}
					else error();
				}).error(error);
			}catch(e){
				error(e);
			}
		}
	};
	(function(){//初始化
		controller.init();
	})();
});