function loadNavBar(pageType, username) {
	console.log(pageType, username);
	var navList = [{
		"name": "登陆",
		"src": "login.html"
	},{
		"name": "注册",
		"src": "register.html"
	},{
		"name": "任务广场",
		"src": "index.html"
	},{
		"name": "个人资料",
		"src": "main.html"
	},{
		"name": username,
		"src": "main.html"
	},{
		"name": "退出",
		"src": "api/user/loginout"
	}];

	if (pageType === 'login') {
		return [navList[0], navList[1], navList[2]];
	} else if (pageType === 'reg') {

		return [navList[0], navList[2]];
	} else if (pageType === 'signed') {

		return [navList[2], navList[3], navList[4], navList[5]];
	} else if (pageType === 'user') {
		
		return [navList[2], navList[4], navList[5]];
	} else {
		return [navList[0],navList[1],navList[2]];
	}
}

var navController = function($scope, $http, $cookies) {
	$scope.username = $cookies.get('username');
	$scope.toExit = true;
	$scope.exitH = function() {console.log('exit');}

	function getUserInfo() {
		$http.post('api/user/info')
		.success(function (data) {
			console.log(data);
			if (data.status == 1) {
				if (pageType == 'login') {
					window.location = "index.html";
				} else {
					$scope.navList = 
					loadNavBar(pageType == 'visitor' ? 'signed' : pageType
						, data.msg.username);
				}
			} else {
				if (pageType == 'signed') {
					pageType = 'visitor';
				} else if (pageType == 'user') {
					window.location = "index.html";
				}
				$scope.navList = loadNavBar(pageType, '');
			}
		});
	}

		console.log(pageType);
	getUserInfo();

/*
	if ($scope.username != undefined) {
		if (pageType == "login") {
			window.location = "index.html";
		}

		//pageType = "signed";
	}
*/
	/*
	if (pageType == "signed") {
		getUserInfo();
	} else {
		$scope.navList = loadNavBar(pageType, $scope.username);
	}*/
}