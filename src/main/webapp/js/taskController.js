function taskController($scope, $http, $rootScope) {
	var taskList = [];// http get /task/list
	$rootScope.taskList = [];
	$scope.taskType = taskType;

	$rootScope.getTask = function(taskType) {
		$http.get('api/task/list/' + taskType).success(function(data) {
			console.log(data);
			loadTask(data);
		})
		.error(function(data) {
			console.log(data);
		});
	}

	$rootScope.getUserTask = function(taskType) {
		$http.get('api/task/list/type/' + taskType)
		.success(function(data){
			console.log(data);
			loadTask(data.msg);
		});
	}
	
	$scope.acceptTask = function (taskId) {
		$http.get('api/task/accept/' + taskId).success(function(data) {
			if (data.status == 1) {
				alert(data.msg);
				$rootScope.taskList = $rootScope.taskList
				.filter(function(task) {
					if (task.task_id != taskId) return true;
				});
			}
			console.log(data);
		});
	}

	$scope.completeTask = function (taskId) {
		$http.get('api/task/complete/' + taskId).success(function(data) {
			if (data.status == 1) {
				
				$rootScope.showMessage(data.msg, true);
				$rootScope.taskList = $rootScope.taskList
				.filter(function(task) {
					if (task.task_id != taskId) return true;
				});
			}
		});
	}

	$scope.cancelTask = function (taskId) {
		$http.get('api/task/cancel/' + taskId).success(function(data) {
			if (data.status == 1) {
				//alert(data.msg);
				$rootScope.showMessage(data.msg, true);
				$rootScope.taskList = $rootScope.taskList
				.filter(function(task) {
					if (task.task_id != taskId) return true;
				});
			}
		});
	}

	if (taskType == 'home') {
		$rootScope.getTask(1);
	} else {
		$rootScope.getUserTask(0);
	}

	function loadTask(data) {
		var myTaskType = $rootScope.taskType;

		for (var i = 0; i < data.length; i++) {
			data[i].img = getImageByType(data[i].category);
			if (myTaskType == 1) {
				var taskInfo = getTaskInfo(data[i].type);

				console.log(taskInfo);
				data[i].taskClass = taskInfo[1];
				data[i].taskInfo = taskInfo[0];
			}
		}

		$rootScope.taskList = data;
	}
}

function getTaskInfo(data) {
	switch (data) {
		case 1:
			return ['等待接受', 'task_wait'];
		case 2:
			return ['正在进行', 'task_accepted'];
		case 3:
			return ['已取消', 'task_cancel'];
		case 4:
			return ['已完成', 'task_complete'];
		default:
			return ['unkonwn', 'unkonwn'];
	}
}

function getImageByType(cate) {
	console.log(cate)
	switch (cate) {
		case '取快递':
			return 'image/kuaidi.png';
		case '辅导':
			return 'image/fudao.png';
		case '约伴儿':
			return 'image/yueban.png';
		default:
			return 'image/qita.png';
	}
}
function getButtonType(status){
    switch (status) {
		case 'acceptd':
			return false;
		default:
			return true;
    }
}