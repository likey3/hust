var index = angular.module('index', ['ngCookies']);

pageType = 'visitor';
taskType = 'home';

index.controller('taskCtrl', taskController)
.controller('navCtrl', navController)
.controller('carouselCtrl',function($scope){

})
.controller('tasktypeCtrl',function($scope, $http, $rootScope){
	$scope.taskCategory = [];

	$http.get('api/task/category/list').success(function(data) {
		$scope.taskCategory = data;
	});

	$scope.getTask = function(taskType) {
		$rootScope.getTask(taskType);
	}
});

$(function(){
    $('.carousel').carousel({
      interval: 3000
    });
});