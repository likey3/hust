var nav = angular.module('register',['ngCookies']);
pageType = 'reg';

nav.controller('registerCtrl',function($scope, $http){
   $scope.username="";
   $scope.tele="";
   $scope.password1="";
   $scope.password2="";
  
  /*未输入用户名就点击下面的输入框*/ 
  $scope.nameVali=function() {     
    if ($scope.username.length<1){
       $scope.noticeuser = "请输入用户名";
      } else {
         $scope.noticeuser = "";    
      }
  }
 /*未输入手机号就点击下面的输入框*/ 
  $scope.teleVali=function() {  
    if ($scope.tele.length<1) {
       $scope.noticetele="请输入手机号";
    } else {
       $scope.noticetele="";  
    }
  }
  /*未输入密码就点击下面的输入框*/ 
  $scope.passwordVali1=function() {  
    if ($scope.password1.length<1){
       $scope.notice1="请输入密码";
    } else {
       $scope.notice1="";
    }
  }

    /*未输入密码确认就点击下面的输入框*/ 
  $scope.passwordVali2=function() {  

    if($scope.password2.length<1){
       $scope.notice2="请输入密码";
    } else {
       $scope.notice2="";    
    }
  }

   $scope.submit = function() {	
    var registerInfo = {
   	"username":$scope.username,
   	"password":$scope.password1,
   	"phonenumber": $scope.tele,
   	"age":"22",
   	"sex":"男",
   	"major":"软件"};
    
    var isOk = true;

    /*未输入用户名就点提交*/
    if ($scope.username.length==0) {
       $scope.noticeuser="请输入用户名";
       isOk = false; 
    }

      /*未输入手机号*/
    if ($scope.tele.length==0){
      $scope.noticetele="请输入手机号";
      isOk = false; 
    } else if ($scope.tele.length!=11){
      $scope.noticetele="手机号码不正确";
      isOk = false; 
    }

    /*密码为空*/
    if($scope.password1.length<1){
      $scope.notice1="请输入密码";
      isOk = false;     
    } /*密码格式不正确*/else if ($scope.password1.length>12 
      || $scope.password1.length<4 ){
      $scope.notice1="密码格式不正确";         
      isOk = false; 
    }   /*两次密码不一致*/ else if ($scope.password1!=$scope.password2){       
      $scope.notice2="两次密码不符";
      isOk = false; 
    }

    if (!isOk) {
        return;
    }
    document.getElementsByClassName("loading")[0].style.display="block";
        

   	$http.post('api/user/register', registerInfo)
   	.success(function(data) {
      console.log(data);
   		if (data.status == 1) {
   			window.location = "index.html";
   		} else if (data.status == 0) {
      document.getElementsByClassName("loading")[0].style.display="none";
       $scope.error_notice=data.msg;
        
      }
   	}).error(function(data) {
   		
   	});
   }
})
.controller('navCtrl', navController);

