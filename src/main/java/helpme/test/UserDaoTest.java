package helpme.test;

import java.sql.Connection;
import java.sql.SQLException;

import helpme.dao.UserDao;
import helpme.dao.imp.UserDaoImp;
import helpme.domin.User;
import helpme.util.ConnectionFactory;

/**
 * 	FileName: UserDaoTest.java
 *	desc: 用户测试UserDAO.
 *	Date: 2016/01/21
 *	Copyright: hust
 *  @author wangxi
 *
 */
public class UserDaoTest {

	public static void main(String[] args) {
		Connection conn = null;

		try {
			conn = ConnectionFactory.getInstance().makeConnection();
			conn.setAutoCommit(false);

			UserDao userDao = new UserDaoImp();
			User tom = new User();

			tom.setUsername("wanghua");
			tom.setPassword("0119");
			tom.setSex("男");
			tom.setAge(22);
			tom.setPhonenumber("1234567890");
			userDao.insert(conn, tom);
			System.out.println("=========保存完毕=========");

			conn.commit();
		} catch (SQLException e) {

			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

}