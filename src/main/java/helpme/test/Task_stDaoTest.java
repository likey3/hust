package helpme.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import helpme.dao.Task_stDao;
import helpme.dao.imp.Task_stDaoImp;
import helpme.domin.Task_State;
import helpme.util.ConnectionFactory;

public class Task_stDaoTest {

	public static void main(String[] args) {
		Connection conn = null;
		try {
			conn = ConnectionFactory.getInstance().makeConnection();
			conn.setAutoCommit(false);
			Task_State ts = new Task_State();
			Task_stDao tst = new Task_stDaoImp();
			Date date=new Date();
			DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time=format.format(date);
			
			ts.setTask_id(1);
			ts.setUsername("wangxi");
			ts.setType(1);
			ts.setTime(time);
			
			tst.insert(conn, ts);
			
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			}
		}
	}

}
