package helpme.test;

import java.sql.Connection;

import helpme.util.ConnectionFactory;

/**
 * @author wangxi
 *
 */
public class ConnectionFactoryTest {

	public static void main(String[] args) throws Exception {
		ConnectionFactory cf = ConnectionFactory.getInstance();

		Connection conn = cf.makeConnection();

		System.out.println(conn.getAutoCommit());

	}

}