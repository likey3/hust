package helpme.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import helpme.service.TaskService;
import helpme.service.imp.TaskServiceImp;
import helpme.domin.Task_State;
import helpme.domin.Task_information;

public class TaskServiceTest {
	public static void main(String[] args) throws Exception {
		TaskService tse = new TaskServiceImp();

		Task_information ti = new Task_information();
		ti.setTitle("干架");
		ti.setContent("啊水水水水水");
		ti.setCategory("asasas");
		
		ti.setUsername("wangxi");
		
		Task_State ts = new Task_State();
		ts.setUsername("wangxi");
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=format.format(date);
		ts.setTime(time);
		
		if(tse.publishTask(ti, ts)){
			System.out.println("发布成功");
		}
		
	}
}
