package helpme.test;

import java.sql.Connection;
import java.sql.SQLException;
import helpme.dao.Task_inDao;
import helpme.dao.imp.Task_inDaoImp;
import helpme.domin.Task_information;
import helpme.util.ConnectionFactory;

public class Task_inDaoTest {

	public static void main(String[] args) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			Task_information ti = new Task_information();
			Task_inDao tin = new Task_inDaoImp();
			ti.setTitle("干架");
			ti.setContent("啊水水水水水");
			ti.setCategory("asasas");
			ti.setUsername("wangxi");
			ti.setType(1);
			tin.publish(conn, ti);
			System.out.println("======发布完毕======");
			
			conn.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}


	}
}
