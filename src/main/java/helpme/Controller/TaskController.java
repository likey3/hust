
package helpme.Controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import helpme.service.*;
import helpme.domin.*;
import helpme.service.imp.TaskServiceImp;
import helpme.service.imp.UserServiceImp;
import helpme.util.TaskUtils;
import helpme.util.UserUtils;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/task")
public class TaskController {
	
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	
	
	@GET @Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonArray getTaskList() {
		TaskService tse = new TaskServiceImp();
		return Task_information.getTaskListJson(tse.listTask(1)).build();
		
	}
	
	@GET @Path("/list/type/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	public  JsonObject getTaskList(@PathParam("type") int type){
		TaskService tse = new TaskServiceImp();
		String username = UserUtils.getSessionUsername(request);
		
		int status = 0;
		String msg = "fail";
		JsonArray listObject = null;
		JsonObjectBuilder result = Json.createObjectBuilder();
		if(username != null){
			if( type >= 1 && type <=4) { 	 	
				status = 1;
				listObject = Task_information.getTaskListJson(tse.listTask(type, username)).build();
				result.add("msg", listObject);
			}else {
				status = 1;
				listObject = Task_information.getTaskListJson(tse.listTask(username)).build();
				result.add("msg", listObject);
			}
			
		}else{
			msg = "logout";
			result.add("msg", msg);
		}
		return result.add("status", status).build();
	}
	
	@GET @Path("/list/{category}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonArray getTaskByCategory(@PathParam("category") int category) {
		TaskService tse = new TaskServiceImp();
		HashMap<Integer, String> hash = TaskUtils.getTaskCategory();
		if(category == 1) {
			return getTaskList();
		}else {
			String categoryValue = hash.get(category);
			return Task_information.getTaskListJson(tse.listTask1(categoryValue)).build();
		}
			
	}	
	
	@GET @Path("/category/list")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonArray getTaskCategory() {
		return TaskUtils.getTaskCategoryJson();
	}
	
	
	
	
	@GET @Path("/detail/{task_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonObject getTaskDetail(@PathParam("task_id") int task_id) {
		TaskService tse = new TaskServiceImp();
		Task_information ti = new Task_information();
		ti = tse.get(task_id);
		return ti.toJson();
	}
	

	
	@POST @Path("/publish")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public JsonObject publish(JsonObject taskInfo){
		TaskService tse = new TaskServiceImp();
		UserService us = new UserServiceImp();
		String username = UserUtils.getSessionUsername(request);
		Task_information ti = new Task_information();
		ti.setTitle(taskInfo.getString("title"));
		ti.setContent(taskInfo.getString("content"));
		ti.setCategory(TaskUtils.getTaskCategory().get(taskInfo.getInt("category")));
		
		int status = 0;
		String msg = "fail";
		if(us.isUserExist(username, null)) {
			ti.setUsername(username);
		}else{
			msg = "用户名不存在";
			return Json.createObjectBuilder()
					.add("msg", msg)
					.add("status", status)
					.build();
		}
		Task_State ts = new Task_State();
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=format.format(date);
		ts.setTime(time);
		ts.setUsername(username);
		
		if(tse.publishTask(ti, ts)){
			status = 1;
			msg = "publish success";
		}else{
			msg = "error";
		}
		return Json.createObjectBuilder()
				.add("msg", msg)
				.add("status", status)
				.build();
	}
	
	@GET @Path("/accept/{task_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonObject accept(@PathParam("task_id") int task_id){
		String username = UserUtils.getSessionUsername(request);
		
		if (username == "") {
			return Json.createObjectBuilder()
					.add("status", 0)
					.add("msg", "请先登录").build();
		}
		TaskService tse = new TaskServiceImp();
		Task_information ti = new Task_information();
		ti.setTask_id(task_id);
		
		Task_State ts = new Task_State();
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=format.format(date);
		ts.setTime(time);
		ts.setTask_id(task_id);
		ts.setUsername(username);
		
		int status = 0;
		String msg = "fail";
		if(tse.acceptTask(ti, ts)){
			status = 1;
			msg = "accept success";
		}else{
			msg = "error";
		}
		return Json.createObjectBuilder()
				.add("msg", msg)
				.add("status", status)
				.build(); 	 	
	}
	
	@GET @Path("/cancel/{task_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonObject cancel(@PathParam("task_id") int task_id){
		String username = UserUtils.getSessionUsername(request);
		
		if (username == "") {
			return Json.createObjectBuilder()
					.add("status", 0)
					.add("msg", "请先登录").build();
		}
		TaskService tse = new TaskServiceImp();
		Task_information ti = new Task_information();
		ti.setTask_id(task_id);
		
		Task_State ts = new Task_State();
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=format.format(date);
		ts.setTime(time);
		ts.setTask_id(task_id);
		ts.setUsername(username);
		
		int status = 0;
		String msg = "fail";
		if(tse.cancelTask(ti, ts)){
			status = 1;
			msg = "cancel success";
		}else{
			msg = "error";
		}
		return Json.createObjectBuilder()
				.add("msg", msg)
				.add("status", status)
				.build();
	}
	
	@GET @Path("/complete/{task_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonObject complete(@PathParam("task_id") int task_id){
		String username = UserUtils.getSessionUsername(request);
		
		if (username == "") {
			return Json.createObjectBuilder()
					.add("status", 0)
					.add("msg", "请先登录").build();
		}
		
		TaskService tse = new TaskServiceImp();
		Task_information ti = new Task_information();
		ti.setTask_id(task_id);
		
		Task_State ts = new Task_State();
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=format.format(date);
		ts.setTime(time);
		ts.setTask_id(task_id);
		ts.setUsername(username);
		
		int status = 0;
		String msg = "fail";
		if(tse.completeTask(ti, ts)){
			status = 1;
			msg = "complete success";
		}else{
			msg = "error";
		}
		return Json.createObjectBuilder()
				.add("msg", msg)
				.add("status", status)
				.build();
	}
}