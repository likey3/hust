package helpme.Controller;

import java.io.IOException;

import helpme.Models.*;
import helpme.domin.User;
import helpme.service.*;
import helpme.service.imp.UserServiceImp;
import helpme.util.UserUtils;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UserController {
	private UserService us = new UserServiceImp();
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	
	
	@POST @Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonObject registerUser(UserRegisterModel registerInfo) {
		User user = new User();
		user.setUsername(registerInfo.username);
		user.setPassword(registerInfo.password);
		user.setPhonenumber(registerInfo.phonenumber);
		
		String msg = "fail";
		int status = 0;
		if(us.registerUser(user)){
			msg = "success";
			status = 1;
			UserUtils.userLogin(request, response, user.getUsername());
		}else{
			msg = "用户名或电话号码已经存在";
		}
		return Json.createObjectBuilder()
				.add("msg", msg)
				.add("status", status)
				.build();
		
		
	}
	
	@POST @Path("/login")
	public JsonObject userLogin(UserLoginModel loginInfo) {
		int status = 0;
		String msg = "fail";
		
		if (us.loginUser(loginInfo.username, loginInfo.password)) {
			status = 1;
			msg = "success";
			String username = us.getUser(loginInfo.username).getUsername();
			UserUtils.userLogin(request, response, username);
		} else {
			status = 0;
			msg = "用户名(电话号码)或密码错误";
		}
		
		return Json.createObjectBuilder()
				.add("status", status)
				.add("msg", msg)
				.build();
	}
	
	@GET @Path("/login")
	public void userLogin() throws IOException {
		response.sendRedirect("../login.html");
	}
		
	@POST @Path("/info")
	public JsonObject getUserInfo() {
		String username = UserUtils.getSessionUsername(request);
		User user = null;
		int status = 0;
		String msg = "fail";
		JsonObject userObject = null;
		JsonObjectBuilder result = Json.createObjectBuilder();
		
		if(username != ""){
			user = us.getUser(username);
			status = 1;
			userObject = user.toJson();
			result.add("msg", userObject);
		}else{
			result.add("msg", msg);
		}
		return result.add("status", status).build();
	}
	
	@POST @Path("/update")
	public JsonObject update(UserUpdateModel updateInfo) {
		User user = new User();
		user.setAge(updateInfo.age);
		user.setSex(updateInfo.sex);
		user.setMajor(updateInfo.major);
		String username = UserUtils.getSessionUsername(request);
		
		int status = 0;
		String msg = "fail";
		if(us.updateUser(username, user)) {
			status = 1;
			msg = "success";
		}else {
			
		}
		
		return Json.createObjectBuilder()
				.add("status", status)
				.add("msg", msg)
				.build();
	}
	
	
	@GET @Path("/loginout")
	public void loginOut() throws IOException {
		UserUtils.UserLoginOut(request, response);
		response.sendRedirect(request.getContextPath() + "/index.html");
	}
	
	@GET @Path("/info")
	public String test() {
		return UserUtils.getSessionUsername(request);
	}
}
