package helpme.service;

import java.util.List;

import helpme.domin.Task_State;
import helpme.domin.Task_information;

public interface TaskService {
	boolean publishTask(Task_information ti, Task_State ts);
	boolean acceptTask(Task_information ti, Task_State ts);
	boolean couldAccept(int task_id, String username);
	boolean cancelTask(Task_information ti, Task_State ts);
	boolean couldCancel(int task_id, String username);
	boolean completeTask(Task_information ti, Task_State ts);
	boolean couldComplete(int task_id, String username);
	List<Task_information> listTask(int type); //根据当前任务状态查询
	List<Task_information> listTask(int type, String username);  //根据当前任务状态和用户名查询
	List<Task_information> listTask(String username);  //根据用户名查询
	List<Task_information> listTask1(String category);  //根据任务类别查询type=1的任务列表
	Task_information get(int task_id);  //根据任务id查询
	Task_State get(int task_id, String username, int type);
}
