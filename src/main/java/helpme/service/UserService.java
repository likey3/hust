package helpme.service;

import helpme.domin.User;

public interface UserService {
	boolean registerUser(User user);
	boolean loginUser(String username, String password);
	boolean updateUser(String username, User user);
	boolean deleteUser(String username) throws Exception;
	boolean isUserExist(String username, String phonenumber);
	User getUser(String username);
	String getusername(String username); 
}
