package helpme.service.imp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import helpme.dao.Task_inDao;
import helpme.dao.Task_stDao;
import helpme.dao.imp.Task_inDaoImp;
import helpme.dao.imp.Task_stDaoImp;
import helpme.domin.Task_State;
import helpme.domin.Task_information;
import helpme.service.TaskService;
import helpme.util.ConnectionFactory;

public class TaskServiceImp implements TaskService {
	
	private Task_inDao tin = new Task_inDaoImp();
	private Task_stDao tst = new Task_stDaoImp();

	@Override
	public boolean publishTask(Task_information ti, Task_State ts) {
		// TODO 自动生成的方法存根
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			int type = 1;
			ti.setType(type);
			ts.setType(type);
			int task_id = tin.publish(conn, ti);
			ts.setTask_id(task_id);
			tst.insert(conn, ts);
			conn.close();
			return true;
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean acceptTask(Task_information ti, Task_State ts) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			if(couldAccept(ti.getTask_id(), ts.getUsername())){
				int type = 2;
				ti.setType(type);
				ts.setType(type);
				tin.accept(conn, ti);
				tst.insert(conn, ts);
				conn.close();
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean couldAccept(int task_id, String username) {
		Task_information ti = new Task_information();
		ti = get(task_id);
		if(ti.getUsername().equals(username) || ti.getType() != 1){
			return false;
		}else{
			return true;
		}
	}
	

	@Override
	public boolean cancelTask(Task_information ti, Task_State ts) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			if(couldCancel(ti.getTask_id(), ts.getUsername())){
				int type = 3;
				ti.setType(type);
				ts.setType(type);
				tin.accept(conn, ti);
				tst.insert(conn, ts);
				conn.close();
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean couldCancel(int task_id, String username) {
		Task_information ti = new Task_information();
		Task_State ts = new Task_State();
		ti = get(task_id);
		ts = get(task_id, username, 2);
		if(ts == null || ti == null)
			return false;
		else{
			
			if(ti.getUsername().equals(username) && ti.getType() == 1){//自己发布的任务自己取消
				return true;
			}else if(ts.getUsername().equals(username) && ti.getType() == 2){//自己接收的任务自己取消
				return true;
			}else{
				return false;
			}
		}
	}

	@Override
	public boolean completeTask(Task_information ti, Task_State ts) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			if(couldComplete(ti.getTask_id(), ts.getUsername())){
				int type = 4;
				ti.setType(type);
				ts.setType(type);
				tin.accept(conn, ti);
				tst.insert(conn, ts);
				conn.close();
				return true;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean couldComplete(int task_id, String username) {
		Task_information ti = new Task_information();
		Task_State ts = new Task_State();
		ti = get(task_id);
		ts = get(task_id, username, 2);
		if(ti == null || ts == null)
			return false;
		else{
			if(ts.getUsername().equals(username) && ti.getType() == 2){
				return true;
			}else{
				return false;
			}
		}
	}
	
	@Override
	public List<Task_information> listTask(int type) {
		List<Task_information> til = new ArrayList<Task_information>();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();	
		try {
			conn.setAutoCommit(false);
			rs = tin.get(conn, type);
			while (rs.next()) {
				Task_information ti = new Task_information();
				ti.setTask_id(rs.getInt(1));
				ti.setUsername(rs.getString(2));
				ti.setTitle(rs.getString(3));
				ti.setContent(rs.getString(4));
				ti.setType(rs.getInt(5));
				ti.setCategory(rs.getString(6));
//				ti.setAcceptusername(rs.getString(9));
				ti.setTime(rs.getString(11));
				til.add(ti);
			}
			conn.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return til;
	}
	
	
	@Override
	public List<Task_information> listTask(int type, String username) {
		List<Task_information> til = new ArrayList<Task_information>();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(false);
			rs = tin.get(conn,type,username);
			while (rs.next()) {
				Task_information ti = new Task_information();
				ti.setTask_id(rs.getInt(1));
				ti.setUsername(rs.getString(2));
				ti.setTitle(rs.getString(3));
				ti.setContent(rs.getString(4));
				ti.setType(rs.getInt(5));
				ti.setCategory(rs.getString(6));
//				ti.setAcceptusername(rs.getString(9));
				ti.setTime(rs.getString(11));
				til.add(ti);
			}
			conn.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return til;
	}
	

	@Override
	public List<Task_information> listTask(String username) {
		List<Task_information> til = new ArrayList<Task_information>();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();	
		try {
			conn.setAutoCommit(false);
			rs = tin.get(conn, username);
			while (rs.next()) {
				Task_information ti = new Task_information();
				ti.setTask_id(rs.getInt(1));
				ti.setUsername(rs.getString(2));
				ti.setTitle(rs.getString(3));
				ti.setContent(rs.getString(4));
				ti.setType(rs.getInt(5));
				ti.setCategory(rs.getString(6));
//				ti.setAcceptusername(rs.getString(9));
				ti.setTime(rs.getString(11));
				til.add(ti);
			}
			conn.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return til;
	}
	
	@Override
	public List<Task_information> listTask1(String category) {
		List<Task_information> til = new ArrayList<Task_information>();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();	
		try {
			conn.setAutoCommit(false);
			rs = tin.get1(conn, category);
			while (rs.next()) {
				Task_information ti = new Task_information();
				ti.setTask_id(rs.getInt(1));
				ti.setUsername(rs.getString(2));
				ti.setTitle(rs.getString(3));
				ti.setContent(rs.getString(4));
				ti.setType(rs.getInt(5));
				ti.setCategory(rs.getString(6));
//				ti.setAcceptusername(rs.getString(9));
				ti.setTime(rs.getString(11));
				til.add(ti);
			}
			conn.close();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return til;
	}




	@Override
	public Task_information get(int task_id) {
		Task_information ti = new Task_information();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(false);
			rs = tin.get1(conn, task_id);
			while(rs.next()){
				ti.setTask_id(rs.getInt(1));
				ti.setUsername(rs.getString(2));
				ti.setTitle(rs.getString(3));
				ti.setContent(rs.getString(4));
				ti.setType(rs.getInt(5));
				ti.setCategory(rs.getString(6));
//				ti.setAcceptusername(rs.getString(9));
				ti.setTime(rs.getString(11));
				conn.close();
				return ti;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return null;
	}
	
	public Task_State get(int task_id, String username, int type){
		Task_State ts = new Task_State();
		ResultSet rs = null;
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(false);
			ts.setTask_id(task_id);
			rs = tst.get(conn, task_id, username, type);
			while(rs.next()){
				ts.setId(rs.getInt(1));
				ts.setTask_id(rs.getInt(2));
				ts.setUsername(rs.getString(3));
				ts.setType(rs.getInt(4));
				ts.setTime(rs.getString(5));
				conn.close();
				return ts;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return null;
		
	}
	


}


