package helpme.service.imp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.dao.UserDao;
import helpme.dao.imp.UserDaoImp;
import helpme.domin.User;
import helpme.service.UserService;
import helpme.util.ConnectionFactory;

public class UserServiceImp implements UserService {

	private UserDao userDao = new UserDaoImp();

	@Override
	public boolean registerUser(User user) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			if(isUserExist(user.getUsername(), user.getPhonenumber())){
				return false;
			}else{
				userDao.insert(conn, user);
				conn.close();
				return true;
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return false;
			
	}

	@Override	
	public boolean loginUser(String s, String password) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(false);
			User user = new User();
			user.setUsername(s);
			user.setPassword(password);
			ResultSet rs = userDao.get(conn, user);
			while (rs.next()) {
				return true;
			}
			ResultSet rs2 = userDao.get(conn, s, password);
			while (rs2.next()) {
				return true;
			}
			
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
			}
		}
		return false;

	}
	
	@Override
	public boolean updateUser(String username, User user) {
		// TODO 自动生成的方法存根
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(true);
			userDao.update(conn, username, user);
			conn.close();
			return true;
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteUser(String username) throws Exception {
		// TODO 自动生成的方法存根
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		conn.setAutoCommit(false);
		User user = new User();
		user.setUsername(username);
		userDao.delete(conn, user);
		conn.close();
		
		return false;
	}

	@Override
	public boolean isUserExist(String username, String phonenumber) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		try {
			conn.setAutoCommit(false);
			ResultSet rs = userDao.get(conn, username);
			while (rs.next()) {
				return true;
			}
			ResultSet rs2 = userDao.get2(conn, phonenumber);
			while (rs2.next()) {
				return true;
			}
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
			}
		}
		return false;
	}

	@Override
	public User getUser(String username) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();

		User users = null;
		try {
			conn.setAutoCommit(false);
			
			ResultSet rs = userDao.get(conn, username);
			while (rs.next()) {
				users = new User();
				users.setUsername(rs.getString(1));
				users.setSex(rs.getString(3));
				users.setAge(rs.getInt(4));
				users.setPhonenumber(rs.getString(5));
				users.setMajor(rs.getString(6));
				System.out.println();
			}
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return users;
	}
	
	public User getUser2(String phonenumber) {
		Connection conn = null;
		conn = ConnectionFactory.getInstance().makeConnection();
		User users = new User();
		try {
			conn.setAutoCommit(false);
			ResultSet rs = userDao.get2(conn, phonenumber);
			while (rs.next()) {
				users.setUsername(rs.getString(1));
				users.setPassword(rs.getString(2));
				users.setSex(rs.getString(3));
				users.setAge(rs.getInt(4));
				users.setPhonenumber(rs.getString(5));
				users.setMajor(rs.getString(6));
			}
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public String getusername(String username) {
		// TODO 自动生成的方法存根
		return null;
	}


}
