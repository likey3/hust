package helpme.domin;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class User {
	private String username;
	private String password;
	private String sex;
	private int age;
	private String phonenumber;
	private String major;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		if (sex == null) sex = "";
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	public String getMajor() {
		return major;
	}
	
	public void setMajor(String major) {
		if (major == null) major = "";
		this.major = major;
	}
	
	public JsonObjectBuilder getJsonBuilder(){
		return Json.createObjectBuilder()
				.add("username", username)
				.add("sex", sex)
				.add("age", age)
				.add("phonenumber", phonenumber)
				.add("major", major);
	}
	
	public JsonObject toJson(){
		return getJsonBuilder().build();
	}

}
