package helpme.domin;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Task_information {
	private int task_id;
	private String username;
	private String title;
	private String content;
	private int type;
	private String category;
	private String time;
	private String acceptusername;

	public int getTask_id() {
		return task_id;
	}

	public void setTask_id(int task_id) {
		this.task_id = task_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAcceptusername() {
		return acceptusername;
	}

	public void setAcceptusername(String acceptusername) {
		this.acceptusername = acceptusername;
	}

	public JsonObjectBuilder getJsonBuilder(){
		return Json.createObjectBuilder()
				.add("username", username)
				.add("title", title)
				.add("content", content)
				.add("task_id", task_id)
				.add("type", type)
				.add("category", category)
//				.add("acceptusername", acceptusername)
				.add("time", time);
		
	}
	
	public JsonObject toJson(){
		return getJsonBuilder().build();
		
	}
	
	public static JsonArrayBuilder getTaskListJson(List<Task_information> til){
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for (int i = 0; i < til.size(); i++) {
			result.add(til.get(i).getJsonBuilder());
		}
		return result;
	}

}
