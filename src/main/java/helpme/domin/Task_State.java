package helpme.domin;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Task_State {
	private int id;
	private int task_id;
	private String username;
	private int type;
	private String time;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTask_id() {
		return task_id;
	}

	public void setTask_id(int task_id) {
		this.task_id = task_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public JsonObjectBuilder getJsonBuilder() {
		return Json.createObjectBuilder()
		.add("id", id)
		.add("task_id", task_id)
		.add("username", username)
		.add("type", type)
		.add("time", time);
	}
	
	public JsonObject toJson() {
		return getJsonBuilder().build();
	}

}
