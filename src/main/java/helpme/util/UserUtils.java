package helpme.util;

import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserUtils {
	public static String getSessionUsername(HttpServletRequest request){
		Object usernameObj = request.getSession().getAttribute("username");
		if (usernameObj != null) {
			return (String)usernameObj;
		}
		return "";
	}
	
	public static void setSession(HttpServletRequest request, String sessionKey, 
			String sessionValue) {
		request.getSession().setAttribute(sessionKey, sessionValue);
	}
	
	public static void clearSession(HttpServletRequest req) {
		Enumeration itr = req.getSession().getAttributeNames();
	
		while (itr.hasMoreElements()) {
			String attrName = (String)itr.nextElement();
			System.out.println(attrName);
			req.removeAttribute(attrName);
			req.getSession().removeAttribute(attrName);
		}
	}
	
	public static void clearCookies(HttpServletRequest req, HttpServletResponse res) {
		if (req.getCookies() == null) return;
		
		for (Cookie cookie : req.getCookies()) {
			cookie.setMaxAge(0);
			cookie.setPath("/");
			
			res.addCookie(cookie);
		}
	}
	
	public static void userLogin(HttpServletRequest request,
			HttpServletResponse res, String username) {
		clearSession(request);
	
		setSession(request, "username", username);
	}
	
	public static void UserLoginOut(HttpServletRequest req, HttpServletResponse res) {
		clearSession(req);
		clearCookies(req, res);
	}
}
