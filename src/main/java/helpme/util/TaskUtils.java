package helpme.util;

import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class TaskUtils {
	public static HashMap<Integer, String> getTaskCategory() {
		HashMap<Integer,String> taskCate = new HashMap<Integer,String>();
		taskCate.put(1, "所有任务");
		taskCate.put(2, "取快递");
		taskCate.put(3, "辅导");
		taskCate.put(4, "约伴儿");
		taskCate.put(5, "其他");
		
		return taskCate;
	}
	public static JsonArray getTaskCategoryJson() {
		HashMap<Integer, String> taskCate = getTaskCategory();
		//JsonObjectBuilder result = Json.createObjectBuilder();
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for (Integer key : taskCate.keySet()) {
			result.add(Json.createObjectBuilder()
					.add("key", key)
					.add("value", taskCate.get(key))
					.build());
		}
		
		return result.build();
	}
}
