package helpme.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.domin.User;

public interface UserDao {
	public boolean insert(Connection conn, User user) throws SQLException;

	public void update(Connection conn, String username, User user) throws SQLException;

	public void delete(Connection conn, User user) throws SQLException;

	public ResultSet get(Connection conn, User user) throws SQLException;
	
	public ResultSet get(Connection conn, String phonenumber, String password) throws SQLException;
	
	public ResultSet get(Connection conn, String username) throws SQLException;
	
	public ResultSet get2(Connection conn, String phonenumber) throws SQLException;
	
}
