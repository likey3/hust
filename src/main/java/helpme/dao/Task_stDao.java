package helpme.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.domin.Task_State;

public interface Task_stDao {
	public void insert(Connection conn, Task_State ts) throws SQLException;

	public void delete(Connection conn, Task_State ts) throws SQLException;

	public ResultSet get(Connection conn, int task_id, String username, int type) throws SQLException;
}
