package helpme.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.dao.Task_stDao;
import helpme.domin.Task_State;

public class Task_stDaoImp implements Task_stDao {

	@Override
	public void insert(Connection conn, Task_State ts) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("INSERT INTO task_state(task_id, username, type, time) values (?,?,?,?)");
		ps.setInt(1,ts.getTask_id());
		ps.setString(2, ts.getUsername());
		ps.setInt(3, ts.getType());
		ps.setString(4, ts.getTime());
		ps.execute();

	}

	@Override
	public void delete(Connection conn, Task_State ts) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("DELETE FROM task_state where time = ?");
		ps.setInt(1, ts.getId());
		ps.execute();
	}

	@Override
	public ResultSet get(Connection conn, int task_id, String username, int type) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_State where task_id = ? AND username = ? AND type =?");
		ps.setInt(1, task_id);
		ps.setString(2, username);
		ps.setInt(3, type);
		return ps.executeQuery();
	}

}
