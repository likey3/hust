package helpme.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import helpme.dao.Task_inDao;
import helpme.domin.Task_information;

public class Task_inDaoImp implements Task_inDao{

	@Override
	public int publish(Connection conn, Task_information ti)
			throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("INSERT INTO task_information(username, title, content, type, category) values (?,?,?,?,?)");
		ps.setString(1, ti.getUsername());
		ps.setString(2, ti.getTitle());
		ps.setString(3, ti.getContent());
		ps.setInt(4, ti.getType());
		ps.setString(5, ti.getCategory());
		ps.execute();
		ResultSet rs = null;
		rs = ps.executeQuery("SELECT LAST_INSERT_ID()"); 
		int autoIncKey = -1;
		if (rs.next()) {
			autoIncKey = rs.getInt(1);
		} else {
			// throw an exception from here
		}
		rs.close();
		rs = null;
		return autoIncKey;
	}

	@Override
	public void accept(Connection conn, Task_information ti)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE task_information SET type = ? where task_id = ?");
		ps.setInt(1, ti.getType());
		ps.setInt(2, ti.getTask_id());
		ps.execute();
	}

	@Override
	public void cancel(Connection conn, Task_information ti)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE task_information SET type = ? where task_id = ?");
		ps.setInt(1, ti.getType());
		ps.setInt(2, ti.getTask_id());
		ps.execute();
		
	}

	@Override
	public void complete(Connection conn, Task_information ti)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("UPDATE task_information SET type = ? where task_id = ?");
		ps.setInt(1, ti.getType());
		ps.setInt(2, ti.getTask_id());
		ps.execute();
		
	}

	@Override
	public ResultSet get(Connection conn, int type)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_information a, task_state b WHERE a.task_id = b.task_id AND a.type = b.type AND a.type = ? ORDER BY b.id DESC");
		ps.setInt(1, type);
		return ps.executeQuery();
	}

	@Override
	public ResultSet get(Connection conn, int type, String username) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_information a, task_state b WHERE a.task_id = b.task_id AND a.type = b.type AND a.type = ? and b.username = ? ORDER BY b.id DESC");
		ps.setInt(1, type);
		ps.setString(2, username);
		return ps.executeQuery();
	}
	
	@Override
	public ResultSet get(Connection conn, String username) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_information a, task_state b WHERE a.task_id = b.task_id AND b.username = ? ORDER BY b.id DESC");
		ps.setString(1, username);
		return ps.executeQuery();
	}
	
	@Override
	public ResultSet get1(Connection conn, String category)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_information a, task_state b where a.task_id = b.task_id AND a.type = b.type AND a.category = ? AND a.type = ?  ORDER BY b.id DESC");
		ps.setString(1, category);
		ps.setInt(2, 1);
		return ps.executeQuery();
	}
	
	@Override
	public ResultSet get1(Connection conn, int task_id)
			throws SQLException {
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM task_information a, task_state b where a.type = b.type AND a.task_id = ?");
		ps.setInt(1, task_id);
		return ps.executeQuery();
	}

}
