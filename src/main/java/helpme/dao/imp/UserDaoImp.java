package helpme.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.dao.UserDao;
import helpme.domin.User;

public class UserDaoImp implements UserDao {

	@Override
	public boolean insert(Connection conn, User user) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("INSERT INTO user(username, password, phonenumber) values (?, ?, ?)");
		ps.setString(1, user.getUsername());
		ps.setString(2, user.getPassword());
		ps.setString(3, user.getPhonenumber());
		return ps.execute();

	}

	@Override
	public void update(Connection conn, String username, User user)
			throws SQLException {
		String updateSql = "UPDATE user SET sex = ?, age = ?, major = ? WHERE username = ?";
		PreparedStatement ps = conn.prepareStatement(updateSql);
		ps.setString(1, user.getSex());
		ps.setInt(2, user.getAge());
		ps.setString(3, user.getMajor());
		ps.setString(4, username);
		ps.execute();

	}

	@Override
	public void delete(Connection conn, User user) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("DELETE FROM user WHERE username = ?");
		ps.setString(1, user.getUsername());
		ps.execute();

	}

	@Override
	public ResultSet get(Connection conn, User user) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("SELECT * FROM user WHERE username = ? AND password = ?");
		ps.setString(1, user.getUsername());
		ps.setString(2, user.getPassword());
		return ps.executeQuery();
	}
	
	public ResultSet get(Connection conn, String phonenumber, String password) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("SELECT * FROM user WHERE phonenumber = ? AND password = ?");
		ps.setString(1, phonenumber);
		ps.setString(2, password);
		return ps.executeQuery();
	}

	@Override
	public ResultSet get(Connection conn, String username) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("SELECT * FROM user WHERE username = ? OR phonenumber = ?");
		ps.setString(1, username);
		ps.setString(2, username);
		return ps.executeQuery();
	}
	
	public ResultSet get2(Connection conn, String phonenumber) throws SQLException {
		PreparedStatement ps = conn
				.prepareStatement("SELECT * FROM user WHERE username = ? OR phonenumber = ?");
		ps.setString(1, phonenumber);
		ps.setString(2, phonenumber);
		return ps.executeQuery();
	}

}
