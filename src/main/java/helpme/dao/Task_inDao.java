package helpme.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import helpme.domin.Task_information;

public interface Task_inDao {
	public int publish(Connection conn, Task_information ti) throws SQLException;

	public void accept(Connection conn, Task_information ti) throws SQLException;
			
	public void cancel(Connection conn, Task_information ti) throws SQLException;
			
	public void complete(Connection conn, Task_information ti) throws SQLException;
			
	public ResultSet get(Connection conn ,int type) throws SQLException; //根据当前任务状态查询
	
	public ResultSet get(Connection conn, int type, String username) throws SQLException;  //根据当前任务状态和用户名查询
	
	public ResultSet get(Connection conn, String username) throws SQLException;   //根据用户名查询
	
	public ResultSet get1(Connection conn, String category) throws SQLException;   //根据任务类别查询type=1的任务列表
	
	public ResultSet get1(Connection conn, int task_id) throws SQLException;  //根据任务id查询

}
